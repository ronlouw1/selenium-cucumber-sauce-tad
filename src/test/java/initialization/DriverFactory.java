/*
 * Defines a driver variable and methods to initialize and clean up the driver.
 * Initializes the driver at the start of a scenario.
 *
 */
package initialization;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author handringa
 */
public class DriverFactory {

    protected static WebDriver driver = null;

    public DriverFactory() {
        initialize();
    }

    private void initialize() {
        // Initializes the geckodriver for the FireFox browser.
        if (driver == null) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            createNewDriverInstance();
        }
    }

    private void createNewDriverInstance() {
        driver = new FirefoxDriver();
    }
        // Starts the FireFox browser.
    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        // Quits the browser and driver.
        driver.quit();
        driver = null;
    }

}
