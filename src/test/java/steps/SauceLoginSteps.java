/*
 * This class contains the definitions of the steps to logon to the website.
 * For each step is a separate method defined annotated with either @Given, @When or @Then.
 * The text in the annotation parameter refers to the step in the feature file.
 *
 */
package steps;

import initialization.DriverFactory;
import static org.junit.Assert.assertEquals;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;
import pages.Menubar;
import pages.ProductsPage;

/**
 *
 * @author handringa
 */
public class SauceLoginSteps extends DriverFactory {

    @Given("^I am on the login page of the sauce demo website$")
    public void i_am_on_the_login_page_of_the_sauce_demo_website() throws Throwable {
        // Calls a method from LoginPage.java to navigate to the login screen.
        
        
    }

    @When("^I login with a locked account$")
    public void i_log_in_with_a_locked_account() throws Throwable {
        // Three actions have to be performed to logon:
        // - Call a method from LoginPage.java that fills in the username
        // - Call a method from LoginPage.java that fills in the password
        // - Call a method from LoginPage.java that pushes the login button


    }

    @Then("I should see an error message")
    public void i_should_see_an_error_message() {
        // The verification step consists of two actions:
        // - Call a method that captures the error message from the screen
        // - Check that the error message is as expected


    }

}
