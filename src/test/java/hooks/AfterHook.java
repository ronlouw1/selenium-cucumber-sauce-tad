/*
 * The defined methodes will be conducted for every scenario.
 * the method annotation @After will be conducted after the scenario.
 */
package hooks;

import initialization.DriverFactory;
import io.cucumber.java.After;

/**
 *
 * @author handringa
 */
public class AfterHook {

    @After
    public void afterScenario() {
        // General actions performed after a scenario. Even after failing.
        new DriverFactory().destroyDriver();
    }

}
